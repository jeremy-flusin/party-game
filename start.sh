#!/bin/bash

npm install
rm -rf dist/
ng build --aot --configuration=production --base-href=/berloul/ --deploy-url=https://flusin.ovh/berloul/
docker-compose up -d
