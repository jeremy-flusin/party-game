export const NUMBER_POINTS_LOST_BY_LOSING = 3;
export const NUMBER_POINTS_WHEN_GUESSING = 1;
export const NUMBER_OF_PASS_ALLOWED = 10;
export const TURN_TIME_SECONDS = 60;
export const NUMBER_OF_TURNS_BY_PLAYER = 5;
export const MIN_PLAYERS = 3;
