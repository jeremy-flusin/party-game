import {NUMBER_OF_PASS_ALLOWED} from './config';

export enum Constraint {
  ONE_TWO_THREE = "Un, deux ou trois mots",
  MIME = "Mime",
  CONCEPTS = "Concepts proches"
}

export interface Challenge {
  value: string;
  subtype: string;
  constraint: Constraint;
}

export enum ChallengeType {
  PEOPLE,
  EXPRESSIONS,
  ARTWORK
}

export interface ChallengeGroup {
  type: ChallengeType;
  label: string;
  values: Array<string>;
}

// ---- ENGINE ----- //
export enum GAME_STATE {
  HOME,
  RULES,
  NEW_GAME,
  PLAYER_START,
  PLAYER_TURN,
  PLAYER_END_OF_TURN,
  END_OF_GAME
}

export class Player {
  name: string;
  score: number;
  passLeft: number;

  constructor(name: string) {
    this.name = name;
    this.score = 0;
    this.passLeft = NUMBER_OF_PASS_ALLOWED;
  }
}

export const mockPlayers = [
  new Player('Aurélien'),
  new Player('Corentin'),
  new Player('Lucas'),
  // new Player('Jérémy'),
  // new Player('Valérian'),
  // new Player('Héloïse'),
]
