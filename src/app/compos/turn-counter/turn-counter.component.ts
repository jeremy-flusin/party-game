import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../model';
import {NUMBER_OF_TURNS_BY_PLAYER} from '../../config';

@Component({
  selector: 'app-turn-counter',
  templateUrl: './turn-counter.component.html',
  styleUrls: ['./turn-counter.component.scss']
})
export class TurnCounterComponent implements OnInit {

  @Input() currentPlayer: Player;
  @Input() turnNumber: number;

  NUMBER_OF_TURNS_BY_PLAYER = NUMBER_OF_TURNS_BY_PLAYER;

  constructor() { }

  ngOnInit() {
  }

}
