import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GAME_STATE} from '../../model';

@Component({
  selector: 'app-home-state',
  templateUrl: './home-state.component.html',
  styleUrls: ['./home-state.component.scss']
})
export class HomeStateComponent implements OnInit {

  @Output() _exitState: EventEmitter<GAME_STATE> = new EventEmitter<GAME_STATE>()

  constructor() { }

  ngOnInit() {}

  startClick(){
    this._exitState.emit(GAME_STATE.NEW_GAME);
  }

  rules(){
    this._exitState.emit(GAME_STATE.RULES);
  }

}
