import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from '../../model';

@Component({
  selector: 'app-g-player-start-state',
  templateUrl: './g-player-start-state.component.html',
  styleUrls: ['./g-player-start-state.component.scss']
})
export class GPlayerStartStateComponent implements OnInit {

  @Input() turnNumber: number;
  @Input() currentPlayer: Player;
  @Output() _exitState: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  readyClick = () => {
    this._exitState.emit();
  }

}
