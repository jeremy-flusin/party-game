import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from '../../model';
import {NUMBER_OF_PASS_ALLOWED} from '../../config';

@Component({
  selector: 'app-g-end-of-game-state',
  templateUrl: './g-end-of-game-state.component.html',
  styleUrls: ['./g-end-of-game-state.component.scss']
})
export class GEndOfGameStateComponent implements OnInit {

  @Input() players: Player[];
  @Output() _exitState: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  isHigherScore = (player: Player): boolean => {
    let highestScore = -100000;
    this.players.forEach((player: Player) => {
      if(player.score > highestScore){
        highestScore = player.score;
      }
    });
    return player.score >= highestScore;
  }

  playAgain = () => {
    this._exitState.emit(true);
  }

  quit = () => {
    this._exitState.emit(false);
  }

}
