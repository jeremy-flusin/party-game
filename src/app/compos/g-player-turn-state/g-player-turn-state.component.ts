import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Challenge, Player} from '../../model';
import {TURN_TIME_SECONDS} from '../../config';

@Component({
  selector: 'app-g-player-turn-state',
  templateUrl: './g-player-turn-state.component.html',
  styleUrls: ['./g-player-turn-state.component.scss']
})
export class GPlayerTurnStateComponent implements OnInit, OnDestroy {

  @Input() currentPlayer: Player;
  @Input() currentChallenge: Challenge;

  @Output() _playerPass: EventEmitter<void> = new EventEmitter<void>();
  @Output() _exitState: EventEmitter<boolean> = new EventEmitter<boolean>();

  currentTimeLeft = TURN_TIME_SECONDS;
  intervalRef = null;

  constructor() { }

  ngOnInit() {
    this.intervalRef = setInterval(() => {
      this.currentTimeLeft -= 1;
      if(this.currentTimeLeft <= 0){
        this.nobodyGuessed();
      }
    }, 1000);
  }

  playerPass = () => {
    this._playerPass.emit();
  }

  somebodyGuessed = () => {
    this._exitState.emit(true);
  }

  abort = () => {
    this._exitState.emit(false);
  }

  nobodyGuessed = () => {
    this._exitState.emit(false);
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalRef);
  }


}
