import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NUMBER_POINTS_LOST_BY_LOSING, NUMBER_POINTS_WHEN_GUESSING} from '../../config';
import {Challenge, Constraint, Player} from '../../model';

@Component({
  selector: 'app-g-player-end-of-turn-state',
  templateUrl: './g-player-end-of-turn-state.component.html',
  styleUrls: ['./g-player-end-of-turn-state.component.scss']
})
export class GPlayerEndOfTurnStateComponent implements OnInit {

  @Input() somebodyGuessed: boolean;
  @Input() currentPlayer: Player;
  @Input() players: Player[];
  @Input() currentChallenge: Challenge;

  @Output() _exitState: EventEmitter<any> = new EventEmitter<any>();


  designatedGuesser: Player = null;
  numberOfWords = 1;

  NUMBER_POINTS_LOST_BY_LOSING = NUMBER_POINTS_LOST_BY_LOSING;
  Constraint = Constraint;

  constructor() { }

  ngOnInit() {}

  imABerloul = () => {
    this._exitState.emit(
      { scoreModifier: -NUMBER_POINTS_LOST_BY_LOSING }
    );
  }

  validateGuess = () => {
    this._exitState.emit(
      {scoreModifier: this.computeGain(), guesser: this.designatedGuesser}
    );
  }

  contest = () => {
    this._exitState.emit(
      { scoreModifier: -NUMBER_POINTS_LOST_BY_LOSING }
    );
  }

  computeGain = () => {
    if(!this.somebodyGuessed){
      return -NUMBER_POINTS_LOST_BY_LOSING;
    }
    if(this.currentChallenge.constraint === Constraint.ONE_TWO_THREE){
      return (4 - this.numberOfWords);
    }else{
      return NUMBER_POINTS_WHEN_GUESSING;
    }
  }

  isDesignatedGuesser = (player: Player) => {
    return player === this.designatedGuesser;
  }

  designateGuesser = (guesser: Player) => {
    this.designatedGuesser = guesser;
  }

  getOpponents = (player: Player): Player[] => {
    const indexOf = this.players.indexOf(player);
    const copy = [].concat(this.players);
    copy.splice(indexOf, 1);
    return [].concat(copy);
  }

  inWords = (words: number) => {
    this.numberOfWords = words;
  }

}
