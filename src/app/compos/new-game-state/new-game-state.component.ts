import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Player} from '../../model';
import {MIN_PLAYERS} from '../../config';

@Component({
  selector: 'app-new-game-state',
  templateUrl: './new-game-state.component.html',
  styleUrls: ['./new-game-state.component.scss']
})
export class NewGameStateComponent implements OnInit {
  MIN_PLAYERS = MIN_PLAYERS;
  playersToCreate: string[];
  @Output() _exitState: EventEmitter<Player[]> = new EventEmitter<Player[]>();

  constructor() { }

  ngOnInit() {
    this.playersToCreate = [];
    for(let i = 0; i < MIN_PLAYERS; i++){
      this.playersToCreate.push('');
    }
  }

  addPlayer = () => {
    this.playersToCreate.push('');
  }

  removePlayer = (playerIndex: number) => {
    this.playersToCreate.splice(playerIndex, 1);
  }

  canRunGame = () => {
    let result = true;
    this.playersToCreate.forEach((playerName: string) => {
      result = result && (playerName !== '');
    });
    return result;
  }

  newGame = () => {
    if(this.canRunGame()){
      const players = [];
      this.playersToCreate.forEach((playerName: string) => {
        players.push(new Player(playerName));
      });
      this._exitState.emit(players);
    }
  }

  // Utilitary method for angular and string tracking
  trackByFn(index: any, item: any) {
    return index;
  }


}
