import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-rules-state',
  templateUrl: './rules-state.component.html',
  styleUrls: ['./rules-state.component.scss']
})
export class RulesStateComponent implements OnInit {

  @Output() _exitState: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  quit = () => {
    this._exitState.emit();
  }
}
