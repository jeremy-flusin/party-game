import {Injectable} from '@angular/core';
import {Challenge, ChallengeGroup, ChallengeType, Constraint} from '../model';
import {CHALLENGE_GROUPS} from '../data';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  constructor() {}

  public getRandomChallenge = (): Challenge => {

    const group: ChallengeGroup = this.getRandomOfArray(CHALLENGE_GROUPS);
    const challengeValue = this.getRandomOfArray(group.values);
    const constraint = this.getRandomOfArray(this.getCompatibleConstraints(group));
    return {
      value: challengeValue,
      subtype: group.label,
      constraint: constraint,
    }
  }

  private getCompatibleConstraints = (challengeGroup: ChallengeGroup): Constraint[] => {
    switch(challengeGroup.type){
      case ChallengeType.PEOPLE:
        return [Constraint.ONE_TWO_THREE, Constraint.MIME];
      case ChallengeType.ARTWORK:
        return [Constraint.ONE_TWO_THREE, Constraint.MIME, Constraint.CONCEPTS];
      case ChallengeType.EXPRESSIONS:
        return [Constraint.MIME, Constraint.CONCEPTS];
    }
  }

  private getRandomOfArray = (array: Array<any>): any => {
    return array[this.getRandomInt(array.length)];
  }

  private getRandomInt = (max: number): number => {
    return Math.floor(Math.random() * max);
  }
}
