import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { HomeStateComponent } from './compos/home-state/home-state.component';
import { NewGameStateComponent } from './compos/new-game-state/new-game-state.component';
import { GPlayerStartStateComponent } from './compos/g-player-start-state/g-player-start-state.component';
import { TurnCounterComponent } from './compos/turn-counter/turn-counter.component';
import { GPlayerTurnStateComponent } from './compos/g-player-turn-state/g-player-turn-state.component';
import { GPlayerEndOfTurnStateComponent } from './compos/g-player-end-of-turn-state/g-player-end-of-turn-state.component';
import { GEndOfGameStateComponent } from './compos/g-end-of-game-state/g-end-of-game-state.component';
import { RulesStateComponent } from './compos/rules-state/rules-state.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeStateComponent,
    NewGameStateComponent,
    GPlayerStartStateComponent,
    TurnCounterComponent,
    GPlayerTurnStateComponent,
    GPlayerEndOfTurnStateComponent,
    GEndOfGameStateComponent,
    RulesStateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
