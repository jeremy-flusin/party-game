import {Component, OnInit} from '@angular/core';
import {Challenge, GAME_STATE, Player} from './model';
import {GeneratorService} from './services/generator.service';
import {NUMBER_OF_TURNS_BY_PLAYER} from './config';
import {environment} from '../environments/environment';
import {version} from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  appVersion = version;

  // ---------- Les STATES --------------
  // HOME : écran d'accueil
  // RULES : règles du jeu
  // NEW_GAME : écran de renseignements des joueurs
  // PLAYER_START : Le joueur est invité à se préparer et à appuyer sur OK
  // PLAYER_TURN : Le tour du joueur est en cours
  // PLAYER_END_OF_TURN : Le joueur doit soit choisir un joueur qui a trouvé, ou cliquer sur "Je suis un berloul"
  // PLAYER_START... ça boucle
  // END_OF_GAME: Fin de partie et résumé des scores !

  GAME_STATE = GAME_STATE;

  players: Player[];

  currentChallenge: Challenge;

  currentState: GAME_STATE;
  currentPlayer: Player;
  turnNumber: number;
  somebodyGuessed: boolean;

  constructor(private generatorService: GeneratorService) {}

  ngOnInit(): void {
    this.currentState = GAME_STATE.HOME;
  }

  private randomizeChallenge = (): Challenge => {
    return this.generatorService.getRandomChallenge();
  }

  exitHomeState = (nextState: GAME_STATE) => {
    this.currentState = nextState;
  }

  exitRuleState = () => {
    this.currentState = GAME_STATE.HOME;
  }

  exitNewGameState = (players: Player[]) => {
    this.players = players;
    this.initNewGame();
    this.currentState = GAME_STATE.PLAYER_START;
  }

  exitPlayerStartState = () => {
    this.currentChallenge = this.randomizeChallenge();
    this.currentState = GAME_STATE.PLAYER_TURN;
  }

  exitPlayerTurnState = (somebodyGuessed: boolean) => {
    this.somebodyGuessed = somebodyGuessed;
    this.currentState = GAME_STATE.PLAYER_END_OF_TURN;
  }

  exitPlayerEndOfTurnState = (recap: any) => {
    this.currentPlayer.score += recap.scoreModifier;
    if(recap.guesser !== undefined){
      this.players[this.players.indexOf(recap.guesser)].score += recap.scoreModifier;
    }
    this.currentState = this.advanceCurrentPlayer() ? GAME_STATE.PLAYER_START : GAME_STATE.END_OF_GAME;
    this.scoreLog();
  }

  exitEndOfGameState = (continuePlaying: boolean) => {
    if(continuePlaying){
      const freshPlayers = [];
      this.players.forEach((player: Player) => {
        freshPlayers.push(new Player(player.name));
      })
      this.players = freshPlayers;
      this.initNewGame();
      this.currentState = GAME_STATE.PLAYER_START;
    }else{
      this.currentState = GAME_STATE.HOME;
    }
  }

  playerPass = () => {
    if(this.currentPlayer.passLeft >= 1){
      this.currentPlayer.passLeft -= 1;
      this.currentChallenge = this.randomizeChallenge();
    }
  }

  private initNewGame = () => {
    this.currentPlayer = this.players[0];
    this.turnNumber = 0;
  }

  private advanceCurrentPlayer = (): boolean /* whether game should continue or not */ => {
    const currentPlayerInd = this.players.indexOf(this.currentPlayer);
    if(currentPlayerInd + 1 >= this.players.length){
      this.currentPlayer = this.players[0];
      this.turnNumber += 1;
      if(this.turnNumber >= NUMBER_OF_TURNS_BY_PLAYER){
        // end game
        return false;
      }
    }else{
      this.currentPlayer = this.players[currentPlayerInd + 1];
    }
    // continue
    return true;
  }

  private scoreLog(){
    if(!environment.production){
      console.log("SCORE RECAP -----");
      this.players.forEach((player: Player) => {
        console.log("  " + player.name + " : " + player.score + " point(s)");
      });
    }
  }

}
